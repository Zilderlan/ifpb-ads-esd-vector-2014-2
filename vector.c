#include "vector.h"
#include <stdlib.h>

Vector* vector_create(int size)
{
    Vector *v = NULL;
    
    /**
     * Insira aqui o código de alocação de memória
     * para a estrutura Vector e para o buffer v->data
     * seguido do código para inicialização correta
     * das demais variáveis da estrutura Vector.
     */
    
    return v;
}

void vector_destroy(Vector* v)
{
   /**
    * Insira aqui o código de liberação de memória
    * para que a estrutura Vector seja totalmente liberada.
    */
}

int vector_insert(Vector* v, int pos, int data)
{
   /**
    * Insira aqui o código para que o elemento 'data'
    * seja inserido na posição 'pos' do vetor.
    * Lembrando que se a posição for maior do que a
    * quantidade de elementos atualmente armazenados
    * no vetor, a inserção deverá acontecer na primeira
    * posição disponível, de modo que o vetor não fique
    * 'banguelo'.
    */
    
    return success;
}

int vector_remove(Vector* v, int pos)
{
   /**
    * Insira aqui o código para que o elemento
    * da posição 'pos' do vetor seja removido do mesmo.
    * Lembre-se de que não se pode remover o que não existe.
    */
    
    return success;
}

int vector_find(Vector* v, int data)
{
   /**
    * Insira aqui o código que encontra a primeira
    * ocorrência do elemento 'data' no vetor.
    * Caso o mesmo não seja encontrado, retornar -1;
    */
    
    return -1;
}

int vector_count(Vector* v, int data)
{
   /**
    * Insira aqui o código que encontra o número
    * de ocorrências do elemento 'data' no vetor.
    */
    
    return 0;
}
