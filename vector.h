#ifndef VECTOR_H
#define VECTOR_H

/* retorna o MENOR valor entre 'a' e  'b' */
#define MIN(a, b) ((a) < (b) ? (a) : (b))

/* retorna o MAIOR valor entre 'a' e  'b' */
#define MAX(a, b) ((a) > (b) ? (a) : (b))

/* Códigos de retorno */
enum {
    success  = 0, /* operação realizada com sucesso.              */
    param_e  = 1, /* erro de parâmetro inválido.                  */
    memory_e = 2, /* erro de alocação de memória.                 */
    buffer_e = 3, /* memória insuficiente para executar operação. */
};

typedef struct {
    int *data;  /* buffer de armazenamento de dados. */
    int  size;  /* capacidade máxima atual.          */
    int  block; /* tamanho do bloco do vetor.        */
    int  used;  /* capacidade utilizada no momento.  */
} Vector;

/**
 * Cria um novo vetor.
 *
 * param [in] block - tamanho do bloco mínimo do vetor.
 *
 * return Vector* em caso de sucesso ou NULL caso contrário.
 */
Vector* vector_create(int block);

/**
 * Destroi um vetor.
 *
 * param [in] v - vetor a ser desalocado.
 */
void vector_destroy(Vector* v);

/**
 * Insere um novo elemento em um vetor.
 *
 * param [in, out] v    - vetor ao qual o dado será inserido.
 * param [in]      pos  - posição da inserção.
 * param [in]      data - valor a ser inserido.
 *
 * return Vide "Códigos de retorno".
 */
int vector_insert(Vector* v, int pos, int data);

/**
 * Remove um elemento de um vetor.
 *
 * param [in, out] v    - vetor ao qual o dado será removido.
 * param [in]      pos  - posição da remoção.
 *
 * return Vide "Códigos de retorno".
 */
int vector_remove(Vector* v, int pos);

/**
 * Busca um elemento em um vetor.
 *
 * param [in, out] v    - vetor no qual o dado será procurado.
 * param [in]      data - valor a ser encontrado.
 *
 * return posição da primeira instância do elemento no vetor
 *        ou -1 caso o elemento não exista no vetor.
 */
int vector_find(Vector* v, int data);

/**
 * Contabiliza a quantidade de um determinado elemento em um vetor.
 *
 * param [in, out] v    - vetor no qual o dado será contabilizado.
 * param [in]      data - valor a ser contabilizado.
 *
 * return quantidade do elemento presente no vetor.
 */
int vector_count(Vector* v, int data);

#endif
